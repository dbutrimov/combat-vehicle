﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game
{
    public class ObjectPoolTimer : MonoBehaviour
    {
        public float duration = 1;

        private float _startTime = 0;

        private void OnEnable()
        {
            _startTime = Time.time;
        }

        private void Update()
        {
            var elapsedTime = Time.time - _startTime;
            if (elapsedTime >= duration)
                gameObject.SetActive(false);
        }
    }
}