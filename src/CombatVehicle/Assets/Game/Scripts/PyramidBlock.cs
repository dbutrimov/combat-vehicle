﻿using DG.Tweening;
using UnityEngine;

namespace Game
{
    public class PyramidBlock : MonoBehaviour
    {
        private Tweener _moveTweener;
        private Tweener _rotateTweener;

        [SerializeField]
        private Rigidbody _rigidbody;
        [SerializeField]
        private Collider _collider;
        [SerializeField]
        private float _size = 1;

        public float Size { get { return _size; } }

        private void Awake()
        {
            if (_rigidbody == null)
                _rigidbody = GetComponent<Rigidbody>();
            if (_collider == null)
                _collider = GetComponent<Collider>();
        }

        public void EnablePhysics(bool enabled)
        {
            _rigidbody.isKinematic = !enabled;
            _collider.enabled = enabled;

            _rigidbody.velocity = Vector3.zero;
            _rigidbody.angularVelocity = Vector3.zero;
        }

        public void Move(Vector3 position, Vector3 rotation, float duration)
        {
            if (_moveTweener != null)
            {
                _moveTweener.Kill();
                _moveTweener = null;
            }

            if (_rotateTweener != null)
            {
                _rotateTweener.Kill();
                _rotateTweener = null;
            }

            EnablePhysics(false);

            _moveTweener = transform.DOMove(position, duration).OnComplete(MoveCompleted);
            _rotateTweener = transform.DORotate(rotation, duration).OnComplete(RotateCompleted);
        }

        void MoveCompleted()
        {
            _moveTweener = null;
            if (_rotateTweener == null)
                MoveAndRotateCompleted();
        }

        void RotateCompleted()
        {
            _rotateTweener = null;
            if (_moveTweener == null)
                MoveAndRotateCompleted();
        }

        void MoveAndRotateCompleted()
        {
            EnablePhysics(true);
        }

        private void OnEnable()
        {
            EnablePhysics(true);
        }

        private void OnDisable()
        {
            if (_moveTweener != null)
            {
                _moveTweener.Kill();
                _moveTweener = null;
            }

            if (_rotateTweener != null)
            {
                _rotateTweener.Kill();
                _rotateTweener = null;
            }

            EnablePhysics(false);
        }
    }
}