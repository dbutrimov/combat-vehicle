﻿using UnityEngine;
using UnityStandardAssets.Vehicles.Car;

namespace Game
{
    public class Speedometer : MonoBehaviour
    {
        [SerializeField]
        private CarController _car;

        [SerializeField]
        private RectTransform _needle;
        
        [SerializeField]
        private float _baseAngle = 0;
        [SerializeField]
        private float _maxAngle = 360;

        [SerializeField]
        private float _minValue = 0;
        [SerializeField]
        private float _maxValue = 160;
        [SerializeField]
        private float _multiplier = 1;


        private void Update()
        {
            var value = _car.CurrentSpeed * _multiplier;
            var normalizedValue = Mathf.Clamp01((value - _minValue) / (_maxValue - _minValue));
            var angle = _baseAngle + _maxAngle * normalizedValue;
            _needle.localRotation = Quaternion.Euler(0, 0, angle);
        }
    }
}
