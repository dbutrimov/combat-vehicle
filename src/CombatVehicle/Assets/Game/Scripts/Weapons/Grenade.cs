﻿using EZObjectPools;
using System;
using UnityEngine;

namespace Game
{
    public class Grenade : Bullet
    {
        private bool _isLaunched = false;

        public float radius = 3f;
        public float power = 5f;
        public float launchForce = 50f;

        public float troubleshootHeight = 0f;

        [SerializeField]
        private Rigidbody _rigidbody;

        private void OnEnable()
        {
            _isLaunched = false;
        }

        public override void Launch(Vector3 direction)
        {
            if (_rigidbody == null)
                _rigidbody = GetComponent<Rigidbody>();

            if (_rigidbody == null)
                throw new InvalidOperationException("Rigidbody is required.");

            _rigidbody.velocity = Vector3.zero;
            _rigidbody.angularVelocity = Vector3.zero;
            _rigidbody.AddForce(direction.normalized * launchForce, ForceMode.Impulse);

            _isLaunched = true;
        }

        void OnTriggerEnter(Collider other)
        {
            if (_isLaunched)
                Explode();
        }

        void Explode()
        {
            var explosionPosition = transform.position;

            // Add the explosion force to nearest objects
            var colliders = Physics.OverlapSphere(explosionPosition, radius);
            foreach (var hit in colliders)
            {
                var rigidbody = hit.GetComponent<Rigidbody>();
                if (rigidbody != null)
                    rigidbody.AddExplosionForce(power, explosionPosition, radius, 3.0f, ForceMode.Impulse);
            }

            gameObject.SetActive(false); // Return the object back to the pool

            // Try to show explosion effect
            var pools = ObjectPools.Instance;
            if (pools != null)
            {
                var explosionPool = pools.GetPool("Explosion");
                if (explosionPool != null)
                    explosionPool.TryGetNextObject(explosionPosition, transform.rotation);
            }
        }

        private void Update()
        {
            if (_isLaunched)
            {
                if (transform.position.y < troubleshootHeight)
                    Explode();
            }
        }
    }
}