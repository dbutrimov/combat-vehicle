﻿using EZObjectPools;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game
{
    public class PyramidManager : MonoBehaviour
    {
        private List<Pyramid> _instantiated;
        private EZObjectPool _blockPool;
        private Coroutine _createCoroutine;

        [SerializeField]
        private Camera _gameCamera;

        public bool createOnStart = true;
        public int createOnStartCount = 20;
        public int minHeight = 5;
        public int maxHeight = 10;
        public float minDistance = 30;
        public float maxDistance = 300;

        protected EZObjectPool BlocksPool
        {
            get
            {
                if (_blockPool == null)
                    _blockPool = ObjectPools.Instance.GetPool("PyramidBlock");

                return _blockPool;
            }
        }
        
        private Pyramid CreatePyramid(Vector3 position, Quaternion rotation, int height)
        {
            var blocksPool = BlocksPool;
            if (blocksPool == null)
                throw new InvalidOperationException("BlockPool is null.");

            var pyramid = Pyramid.Create(position, rotation, height, blocksPool, _gameCamera);
            return pyramid;
        }

        public void Create(int count)
        {
            // Abort active creation process
            if (_createCoroutine != null)
            {
                StopCoroutine(_createCoroutine);
                _createCoroutine = null;
            }

            var routine = CreateCoroutine(count, CreateCoroutineCompleted);
            _createCoroutine = StartCoroutine(routine);
        }

        private IEnumerator CreateCoroutine(int count, Action completed)
        {
            if (_instantiated == null)
                _instantiated = new List<Pyramid>(count);

            for (int i = 0; i < count; i++)
            {
                var height = UnityEngine.Random.Range(minHeight, maxHeight);
                var radius = Pyramid.CalcRadius(height);

                var position = Vector3.zero;
                bool isFree = true;
                do
                {
                    var distance = UnityEngine.Random.Range(minDistance, maxDistance);
                    var direction = UnityEngine.Random.Range(0f, 360f);
                    position = Quaternion.Euler(0, direction, 0) * new Vector3(0, 0, distance);

                    for (int n = 0; n < _instantiated.Count; n++)
                    {
                        var other = _instantiated[n];
                        var dist = Vector3.Distance(position, other.transform.position) - other.Radius - radius;
                        isFree = dist > 0f;
                        if (!isFree)
                            break;
                    }
                }
                while (!isFree);

                var pyramid = CreatePyramid(position, Quaternion.identity, height);
                _instantiated.Add(pyramid);
                
                yield return pyramid.Load();
            }

            if (completed != null)
                completed.Invoke();

            yield return null;
        }

        private void CreateCoroutineCompleted()
        {
            _createCoroutine = null;
        }

        public void Clear()
        {
            if (_instantiated != null)
            {
                for (int i = 0; i < _instantiated.Count; i++)
                    _instantiated[i].Dispose();

                _instantiated = null;
            }
        }

        public void RestoreAll()
        {
            if (_instantiated != null)
            {
                for (int i = 0; i < _instantiated.Count; i++)
                    _instantiated[i].Restore();
            }
        }

        private void Start()
        {
            if (createOnStart)
                Create(createOnStartCount);
        }
    }
}