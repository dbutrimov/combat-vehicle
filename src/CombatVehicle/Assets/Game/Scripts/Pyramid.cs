﻿using EZObjectPools;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game
{
    public class Pyramid : MonoBehaviour
    {
        public static Pyramid Create(Vector3 position, Quaternion rotation, int height, EZObjectPool blockPool, Camera camera)
        {
            if (height <= 0)
                throw new ArgumentOutOfRangeException("height", "Height must be greater than zero.");
            if (blockPool == null)
                throw new ArgumentNullException("blockPool");
            if (camera == null)
                throw new ArgumentNullException("camera");

            var pyramidObj = new GameObject("Pyramid");
            pyramidObj.transform.position = position;
            pyramidObj.transform.rotation = rotation;

            var pyramid = pyramidObj.AddComponent<Pyramid>();
            pyramid.Configure(height, blockPool, camera);
            return pyramid;
        }

        public static float CalcRadius(int height)
        {
            return Mathf.Pow(Mathf.Pow(height, 2f) * 2f, 0.5f) * 0.5f;
        }



        private Coroutine _loadCoroutine;

        private EZObjectPool _blockPool;
        private List<PyramidBlock> _blocks;
        private int _height;
        private Camera _camera;
        private bool _isOnScreen = true;
        private float _radius = 0;

        public float Radius { get { return _radius; } }

        private void Configure(int height, EZObjectPool blockPool, Camera camera)
        {
            if (height <= 0)
                throw new ArgumentOutOfRangeException("height", "Height must be greater than zero.");
            if (blockPool == null)
                throw new ArgumentNullException("blockPool");
            if (camera == null)
                throw new ArgumentNullException("camera");

            _height = height;
            _blockPool = blockPool;
            _camera = camera;
            _radius = CalcRadius(_height);
        }

        public YieldInstruction Load()
        {
            if (_loadCoroutine != null)
            {
                StopCoroutine(_loadCoroutine);
                _loadCoroutine = null;
            }

            var routine = LoadAsync(LoadCoroutineCompleted);
            _loadCoroutine = StartCoroutine(routine);

            return _loadCoroutine;
        }

        private void LoadCoroutineCompleted()
        {
            _loadCoroutine = null;
        }

        const float blockSizeTroubleshoot = 0.001f;

        public IEnumerator LoadAsync(Action completed = null)
        {
            _blocks = new List<PyramidBlock>(30);
            for (int level = 0; level < _height; level++)
            {
                var levelSize = _height - level;
                for (int i = 0; i < levelSize * levelSize; i++)
                {
                    GameObject blockObj = null;
                    if (_blockPool.TryGetNextObject(Vector3.zero, Quaternion.identity, out blockObj))
                    {
                        var block = blockObj.GetComponent<PyramidBlock>();
                        var blockSize = block.Size + blockSizeTroubleshoot;

                        float offset = levelSize * blockSize * 0.5f;

                        float x = (i % levelSize) * blockSize - offset;
                        float y = level * blockSize + blockSize * 0.5f;
                        float z = (int)(i / levelSize) * blockSize - offset;

                        block.transform.rotation = transform.rotation;
                        block.transform.position = transform.position + transform.rotation * new Vector3(x, y, z);
                        block.gameObject.SetActive(true);
                        block.EnablePhysics(false);

                        _blocks.Add(block);
                    }
                }

                yield return new WaitForEndOfFrame(); // TODO: Causes explosion of the pyramid. Must be fixed. Some logic issue.
            }

            for (int i = 0; i < _blocks.Count; i++)
                _blocks[i].EnablePhysics(true);

            if (completed != null)
                completed.Invoke();

            yield return null;
        }

        public void Restore()
        {
            if (_loadCoroutine != null)
                return;

            if (_blocks == null)
                throw new InvalidOperationException();

            int blockIndex = 0;
            for (int level = 0; level < _height; level++)
            {
                var levelSize = _height - level;
                for (int i = 0; i < levelSize * levelSize; i++)
                {
                    var block = _blocks[blockIndex++];
                    var blockSize = block.Size + blockSizeTroubleshoot;

                    float offset = levelSize * blockSize * 0.5f;

                    float x = (i % levelSize) * blockSize - offset;
                    float y = level * blockSize + blockSize * 0.5f;
                    float z = (int)(i / levelSize) * blockSize - offset;

                    var rotation = transform.rotation.eulerAngles;
                    var position = transform.position + transform.rotation * new Vector3(x, y, z);

                    block.Move(position, rotation, 1f);

                    _blocks.Add(block);
                }
            }
        }

        public void Dispose()
        {
            if (_loadCoroutine != null)
            {
                StopCoroutine(_loadCoroutine);
                _loadCoroutine = null;
            }

            if (_blocks != null)
            {
                foreach (var block in _blocks)
                    block.gameObject.SetActive(false); // Return object back to the pool

                _blocks = null;
            }

            GameObject.Destroy(gameObject);
        }

        private void Update()
        {
            Vector3 screenPoint = _camera.WorldToViewportPoint(transform.position);
            bool isOnScreen = screenPoint.z > 0 && screenPoint.x > 0 && screenPoint.x < 1 && screenPoint.y > 0 && screenPoint.y < 1;

            if (isOnScreen != _isOnScreen)
            {
                _isOnScreen = isOnScreen;
                if (_isOnScreen)
                    OnEnterScreen();
                else
                    OnLeaveScreen();
            }
        }

        private void OnLeaveScreen()
        {
            Restore();
        }

        private void OnEnterScreen()
        {

        }
    }
}