## Used Assets

#### Standard Assets
https://assetstore.unity.com/packages/essentials/asset-packs/standard-assets-32351

* 3D models
* Car Controller
* Following Camera
* Mobile Joystick
* Visual effects

#### EZ Object Pools
https://assetstore.unity.com/packages/tools/ez-object-pools-28002

* Pooling of bullets (grenades) and explosion effects

#### DOTween
https://assetstore.unity.com/packages/tools/animation/dotween-hotween-v2-27676

* Animated restoring of pyramids
