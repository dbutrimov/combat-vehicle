﻿using EZObjectPools;
using UnityEngine;

namespace Game
{
    public class ObjectPools : MonoBehaviour
    {
        private static ObjectPools _instance = null;
        public static ObjectPools Instance { get { return _instance; } }
        
        [SerializeField]
        private EZObjectPool[] _registered = null;

        private void Awake()
        {
            if (_instance != null)
            {
                // Destroy current if already exists
                GameObject.DestroyImmediate(gameObject);
                return;
            }

            _instance = this;

            if (_registered != null)
            {
                for (int i = 0; i < _registered.Length; i++)
                    _registered[i].gameObject.SetActive(true);
            }
        }

        private void OnDestroy()
        {
            if (_instance == this)
                _instance = null;
        }

        public EZObjectPool GetPool(string poolName)
        {
            if (_registered != null)
            {
                for (int i = 0; i < _registered.Length; i++)
                {
                    var pool = _registered[i];
                    if (pool.PoolName == poolName)
                        return pool;
                }
            }

            return null;
        }
    }
}