﻿using UnityEngine;

namespace Game
{
    public abstract class Bullet : MonoBehaviour
    {
        public abstract void Launch(Vector3 direction);
    }
}