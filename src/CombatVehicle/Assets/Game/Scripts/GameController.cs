﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace Game
{
    public class GameController : MonoBehaviour
    {
        [SerializeField]
        private PyramidManager _pyramids = null;

        public void ResetGame()
        {
            _pyramids.Clear();

            SceneManager.LoadScene(0);
        }
    }
}