﻿using EZObjectPools;
using UnityEngine;

namespace Game
{
    public abstract class Weapon : MonoBehaviour
    {
        [SerializeField]
        private string _bulletPoolName;

        private EZObjectPool _bulletPool;

        public void Shot()
        {
            if (_bulletPool == null)
                _bulletPool = ObjectPools.Instance.GetPool(_bulletPoolName);

            GameObject bulletObj = null;
            if (_bulletPool.TryGetNextObject(transform.position, transform.rotation, out bulletObj))
            {
                bulletObj.SetActive(true);

                var bullet = bulletObj.GetComponent<Bullet>();
                bullet.Launch(transform.forward);
            }
        }
    }
}